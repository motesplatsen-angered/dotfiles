#!/bin/bash

pgrep mumble || mumble &
pgrep open-stage-control || open-stage-control \
	--load ~/.config/open-stage-control/surfaces/default.json \
	--authentication "$(cat ~/.credentials/osc)" \
	--theme nord \
	--remote-root ~/.config/open-stage-control/surfaces \
	--remote-saving 127.0.0.1 \
	--midi OSC-hw:UMC1820,UMC1820 &
sleep 5
pgrep carla || carla ~/.config/carla/default.carxp &
pgrep obs || obs &
pgrep openlp || openlp &
